<?php
class Sauron extends Characters
{
  public function __construct($name)
  {
    parent::__construct(1000, 200, 400, "aragorn", "sauron", 1, $name);
  }
  
  public function kickAss($enemy, $a, $b) {
    $dmgdone = parent::kickAss($enemy, $a, $b);
    $picon=0;
      foreach($a as $alive){
        if($alive->HP > 0){
          $picon++;
        }
      }
    if ($picon == 3) {
      $dmgdone = round($dmgdone*1.5);
    }
    return $dmgdone;
  }

}