<?php

class MainController 
{
  public function home() {
    include 'view/home.php';
  }

  public function start() {
    include 'assets/start.php';
  }

  public function result() {
    include 'view/result.php';
  }
}

?>