<?php
class Router 
{
  private static $extendRoot = [
    'css' => 'text/css', 
    'js' => 'application/javascript',
    'png' => 'image/png',
    'jpg' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
    'svg' => 'image/svg+xml',
  ];

  public function route(array $routeslist)
  {
    $parseUri = parse_url($_SERVER['REQUEST_URI']);
    if (array_key_exists($parseUri['path'], $routeslist)) {
      $routeslist[$parseUri['path']]();
    } else {
      $extension = pathinfo($parseUri['path'], PATHINFO_EXTENSION);
      $documentroot = $_SERVER['DOCUMENT_ROOT'];
      if (array_key_exists($extension, Router::$extendRoot)) {
        header('Content-type: '.Router::$extendRoot[$extension]);
      }
      $content = file_get_contents($documentroot. DIRECTORY_SEPARATOR . $parseUri['path']);
      echo $content;
    }
  }
}