<?php

class Goblin extends Characters
{
  public function __construct($name)
  {
    parent::__construct(250, 50, 200, "warrior", "goblin", 1, $name);
  }

  /*Fonction pour coup spécial Gobelin
    public function getBuddies($b){
    $picon=0;
    foreach($b as $Perso){
      if($Perso->role()=="goblin" && $Perso->HP() > 0){
        $picon++;
      }
    }
    if ($picon >= 2) {
      $this->setMinSP(100);
      $this->setMaxSP(400);
    } else {
      $this->setMinSP(50);
      $this->setMaxSP(200);
    }
  }*/

  public function kickAss($enemy, $a, $b) {
    $dmgdone = parent::kickAss($enemy, $a, $b);
    $picon=0;
      foreach($b as $Perso){
        if($Perso->role()=="goblin" && $Perso->HP() > 0){
          $picon++;
        }
      }
    if ($picon >= 2) {
      $dmgdone = $dmgdone*2;
    }
    return $dmgdone;
  }

}