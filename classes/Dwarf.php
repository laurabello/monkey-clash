<?php

class Dwarf extends Characters
{
  public function __construct($name)
  {
    parent::__construct(450, 100, 150, "goblin", "dwarf", 1, $name);
  }
}