<?php
abstract class Characters
{
  protected $HP;
  protected $minSP;
  protected $maxSP;
  protected $specialty;
  protected $role;
  protected $level;
  protected $name;

  public function __construct($HP, $minSP, $maxSP, $specialty, $role, $level, $name)
  {
    $this->HP=$HP;
    $this->minSP=$minSP;
    $this->maxSP=$maxSP;
    $this->specialty=$specialty;
    $this->role=$role;
    $this->level=$level;
    $this->name=$name;
  }


  //getters
  public function HP()
  {
    return $this->HP;
  }

  public function minSP()
  {
    return $this->minSP;
  }

  public function maxSP()
  {
    return $this->maxSP;
  }

  public function specialty()
  {
    return $this->specialty;
  }

  public function role()
  {
    return $this->role;
  }

  public function level()
  {
    return $this->level;
  }

  public function name()
  {
    return $this->name;
  }

    //setters
  public function setHP($new_HP)
  {
    $this->HP=$new_HP;
  }

  public function setMinSP($new_minSP)
  {
    $this->minSP=$new_minSP;
  }

  public function setMaxSP($new_maxSP)
  {
    $this->maxSP=$new_maxSP;
  }

  public function setSpecialty($new_specialty)
  {
    $this->specialty=$new_specialty;
  }

  public function setLvl($new_level)
  {
    $this->level=$new_level;
  }

  /*methods
  public function kickAss($enemy) {
    $minDmg = $this->minSP();
    $maxDmg = $this->maxSP();
    $level = $this->level();
    $specialtyAtk = $this->specialty();
    $roleEnemy = $enemy->role();
    if($specialtyAtk == $roleEnemy){
      $dmgdone = round((rand($minDmg,$maxDmg))*1.5)+ 50*($level -1);
    } else {
      $dmgdone = round(rand($minDmg,$maxDmg))+ 50*($level -1);
    }
    $HPLeft = $enemy->HP() - $dmgdone;
    $enemy->setHP($HPLeft);
    if ($HPLeft <= 0) {
      $this->setLvl($level+1);
    }
    echo "<p>Wouuush! ".$this->name()." kicked ".$enemy->name()."'s ass for" .$dmgdone. "! He's down to " .$HPLeft." health points.</p>";
    return $dmgdone;
  }*/

    public function kickAss($enemy, $a, $b) {
      $spec = 1;
      if($enemy INSTANCEOF $this->specialty) {
        $spec = 1.5;
      }
      $dmgdone = round((rand($this->minSP,$this->maxSP))*$spec)+ 50*($this->level -1);
      /*echo $this->role()." says : take that you dirty ".$enemy->role(). "! ";
      echo $this->specialty()." is my specialty, I eat 'em for diner!</br>";*/
      return $dmgdone;
    }

    public function damnIt($attacker, $dmgdone) {
      $HPLeft = $this->HP() - $dmgdone;
      if ($HPLeft < 0) {
        $this->setHP(0);
        $lvlAttack = $attacker->level();
        $attacker->setLvl($lvlAttack+1);
      } else {
        $this->setHP($HPLeft);
        }
      }
      /*echo $this->role()." says : ".$dmgdone."HP? Damnit! Come on you freaking ".$attacker->role().", I'll show you! ";
      echo $this->HP().'HP left</br></br>';*/

}
