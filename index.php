<?php

function uploadClass($class)
{
  require "classes/" . $class . ".php";
}
spl_autoload_register('uploadClass');


Router::route(
  [
    '/' => 'MainController::home',
    '/home' => 'MainController::home',
    '/start' => 'MainController::start',
    '/result' => 'MainController::result'
  ]
)

?>