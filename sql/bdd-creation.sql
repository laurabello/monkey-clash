CREATE TABLE IF NOT EXISTS characters (id_character INTEGER AUTO_INCREMENT NOT NULL,
name_character VARCHAR(50),
level_character INTEGER,
HP_character INTEGER,
id_type INTEGER, 
id_user INTEGER,
PRIMARY KEY (id_character)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS roles (id_role INTEGER AUTO_INCREMENT NOT NULL,
name_role VARCHAR(50),
specialty_role VARCHAR(50),
strenghtMin_role INT,
strenghtMax_role INT,
startHP_role INT,
side_character VARCHAR(50),
PRIMARY KEY (id_role)) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS users (id_user INTEGER AUTO_INCREMENT NOT NULL,
pseudo_User VARCHAR(50),
password_User VARCHAR(70),
PRIMARY KEY (id_user)) ENGINE=InnoDB; 


ALTER TABLE characters ADD CONSTRAINT FK_characters_id_type FOREIGN KEY (id_type) REFERENCES roles (id_role);

ALTER TABLE characters ADD CONSTRAINT FK_characters_id_user FOREIGN KEY (id_user) REFERENCES users (id_user);

