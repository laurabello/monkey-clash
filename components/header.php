<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="Jouez à Simplon War">
  <link rel="icon" href="medias/icons/favicon.png">
  <link rel="stylesheet" href="styles/styles.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <?php echo $path; ?>
  <title><?php echo $title; ?></title>
</head>
<body>  
<header>
    <div class="title-part">
      <div class="logo-title">
        <a href="home">
          <img class="logo" src="./medias/icons/monkey-logo.svg" alt="">
        </a>
        <h1 class="title">Monkeys clash</h1>
      </div>      
      <a class="start-button" href="start">Démarrer une partie</a>
    </div>
  </header>