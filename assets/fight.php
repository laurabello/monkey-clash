<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
 
/**
*recupération du formulaire pour gérer les teams
*
*/

$heroName = $_POST['heroes'];
$heroRole = $_POST['hero-role'];
 
 
$monsterName = $_POST['monsters'];
$monsterRole = $_POST['monster-role'];
 
 
/**
*Gestion de la creation des teams
*
*/
 
function team($a, $b) {
  $c=[];
  for($i = 0; $i < count($a);$i++){
   $perso = new $b[$i]($a[$i]);
   array_push($c, $perso);
 
 }
 return $c;
}
 
 
 
$teamHeroes = team($heroName, $heroRole);
$teamMonsters = team($monsterName, $monsterRole);
 
/*
echo "<pre>";
var_dump($teamHeroes);
 
echo "</pre>";
 
*/
 
// Lancement du combat
 
 
function getFirstLiving($PersoList){
  foreach($PersoList as $Perso){
    if($Perso->HP() > 0){
      return $Perso;
    }
  }
  return NULL;
}
 
 
/**
*
*
*/
 
 
 
function updateTeams($PersoList) {
  $updateTeam = [];
  foreach ($PersoList as $Perso) {
    $updateTeam[] = [
      'HPPerso' => $Perso->HP(),
      'LvlPerso' => $Perso->level(),
      'NamePerso' => $Perso->name(),
      'RolePerso' => $Perso->role()
    ];
  }
  return $updateTeam;
}
 
/**
*Fonction rounds, qui prend le modulo de $i et qui attribue l'attaquant et 
* l'ennemi a chaque tour pour excécuter la fonction kickAss
*
*/
 
function rounds($i, &$a, &$b)
{
	
  if ($i%2 == 0) 
  {
    $attacker=getFirstLiving($b);
    //echo "attaquant: ".$attacker->name()."</br>";
    $enemy=getFirstLiving($a);
    //echo "enemy: ".$enemy->name()."</br>";
  } else {
    $attacker=getFirstLiving($a);
    //echo "attaquant: ".$attacker->name()."</br>";
    $enemy=getFirstLiving($b);
    //echo "enemy: ".$enemy->name()."</br>";
  }
  /*if ($attacker->role()=="goblin") {
    $attacker->getBuddies($b);
  }
  if ($attacker->role()=="sauron") {
    $attacker->megaBlast($a);
  }*/
  $dmgdone = $attacker->kickAss($enemy, $a, $b);
  $enemy->damnIt($attacker, $dmgdone);

  $newTurn = [
    'nameAttacker' => $attacker->name(),
    'nameEnemy' => $enemy->name(),
    'damage' => $dmgdone
  ];
 
  return $newTurn;
}
 
/**
*fonction de fight, qui excecute la fonction de tours tant que le 
*tableau $teamHeroes ou $teamMonster est différent de NULL
*
*/
 
 
function fight($a, $b) {
  $i = 1;
  $end = 0;
  $affichage = array();
  $winner = "";
  $winnerSentence = "";
 
  do {
    $rounds = rounds($i, $a, $b);
    $turnHeroes = updateTeams($a);
    $turnMonsters = updateTeams($b);
    /*
    echo "<pre>";
    var_dump($turnHeroes);
    echo "</pre>";
    */
     
    $LH = getFirstLiving($a);
    $LM =getFirstLiving($b);
    if(!$LM) {
      $end = 1;
      $winner = 'Chay!';
      $winnerSentence = 'Les héros gagnent!';
    //echo "hero gagnent";
    }
    elseif (!$LH) {
      $end = 1;
      $winner = 'Pas glop...';
      $winnerSentence = 'Les monstres gagnent :-(';
    //echo "monstres gagnent";
    }  
    array_push($affichage, ['rounds' => $rounds, 'monsters' => $turnMonsters, 'heroes' => $turnHeroes, 'winner' => $winner, 'winnerSentence' => $winnerSentence]);

 /*
    echo "<pre>";
    var_dump($affichage);
    echo "</pre>";
    */
    $i++;
  }
  while ($end != 1);
  return $affichage;
}

 /*
$displayFight = fight($teamHeroes, $teamMonsters);

var_dump($displayFight);
 
echo 'zboub';
foreach ($displayFight as $turn) {
    print_r($turn);
    echo "</br>";
}
*/
    /*$path = "";
    $title = "Monkey Clash | Issue de la bataille!";
    include "components/header.php";
  ?>
  <main>
    <div class="ribbon">
    </div>
    <h2 class="subtitle">Déroulé du combat</h2>
    <div class="line-sub">
    </div>
 
 
    <div class="turn">
 
     <?php 
     $i =1;
     
     foreach ($displayFight as $turn) {
      
      echo '<div class="action"><div class="actionNumber">Action '.$i.'</div>';
      echo '<p>'.$turn['rounds']['nameAttacker'].' attaque '.$turn['rounds']['nameEnemy'].'</p><img src="medias/icons/arrow.svg" width="30" height="30"></div>';
      echo '<div class="damage"><p>Coup porté: '.$turn['rounds']['damage'].'</p></div>';
      echo '<div class="teams"><div class="heroesTab"><h1>Héros:</h1><table><thead>
      <tr>
      <th>Nom</th>
      <th>Type</th>
      <th>Niveau</th>
      <th>Vie</th>
      </tr>
      </thead>
      <tbody>';
      foreach ($turn['heroes'] as $hero) {
        echo '<tr>';
        echo '<td>'.$hero['NamePerso'].'</td>';
        echo '<td>'.$hero['RolePerso'].'</td>';
        echo '<td>'.$hero['LvlPerso'].'</td>';
        echo '<td>'.$hero['HPPerso'].'</td>';
        echo '</tr>';
      }
      echo '</tbody></table></div>';
 
      echo '<div class="monstersTab"><h1>Monstre:</h1><table><thead>
      <tr>
      <th>Nom</th>
      <th>Type</th>
      <th>Niveau</th>
      <th>Vie</th>
      </tr>
      </thead>
      <tbody>';
      foreach ($turn['monsters'] as $monster) {
        echo '<tr>';
        echo '<td>'.$monster['NamePerso'].'</td>';
        echo '<td>'.$monster['RolePerso'].'</td>';
        echo '<td>'.$monster['LvlPerso'].'</td>';
        echo '<td>'.$monster['HPPerso'].'</td>';
        echo '</tr>';
      }
      echo '</tbody></table></div></div>';
      $i++;
      
    }
 
 
    ?>
    
  </div>
</main>   
<?php
    include "components/footer.php";*/
  ?>