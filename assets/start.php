<?php

  $path = "";
  $title = "Monkeys Clash | Créez vos équipes";
  include "components/header.php";
  ?>
  <main>
    <div class="ribbon">
    </div>
    <audio src="./medias/sounds/SF-snare.mp3" preload="auto" autoplay style="display: none;"></audio>
    <h2 class="subtitle">Créez vos équipes</h2>
    <div class="line-sub">
    </div>
    <form method="post" action="result" class="formu">
      <div class="orga">
        <div class="heroes">
          <p>Les héros</p>
          <div class="perso">
            <p>Personnage 1</p>
            <div class="specs">
              <div class="spe">
                <label for="hero-one-name">Nom*</label>
                <input id="hero-one-name" type="text" name="heroes[]">          
              </div>
              <div class="spe">
               <label for="hero-one-role">Type*</label>
               <select id="hero-one-role" name="hero-role[]">
                <option value="">Choisis le rôle de ton héros</option>
                <option value="Magician">Magicien</option>
                <option value="Elf">Elfe</option>
                <option value="Dwarf">Nain</option>
                <option value="Warrior">Guerrier</option>
              </select>
            </div>
          </div>
        </div>

        

        <div class="perso">
          <p>Personnage 2</p>
          <div class="specs">
            <div class="spe">
              <label for="hero-one-name">Nom*</label>
              <input id="hero-one-name" type="text" name="heroes[]">          
            </div>
            <div class="spe">
             <label for="hero-one-role">Type*</label>
             <select id="hero-one-role" name="hero-role[]">
              <option value="">Choisis le rôle de ton héros</option>
              <option value="Magician">Magicien</option>
              <option value="Elf">Elfe</option>
              <option value="Dwarf">Nain</option>
              <option value="Warrior">Guerrier</option>
            </select>
          </div>
        </div>
      </div>

      <div class="perso">
        <p>Personnage 3</p>
        <div class="specs">
          <div class="spe">
            <label for="hero-one-name">Nom*</label>
            <input id="hero-one-name" type="text" name="heroes[]">          
          </div>
          <div class="spe">
           <label for="hero-one-role">Type*</label>
           <select id="hero-one-role" name="hero-role[]">
            <option value="">Choisis le rôle de ton héros</option>
            <option value="Magician">Magicien</option>
            <option value="Elf">Elfe</option>
            <option value="Dwarf">Nain</option>
            <option value="Warrior">Guerrier</option>
          </select>
        </div>
      </div>
    </div>

  </div>
  <div class="heroes">
    <p>Les monstres</p>
    <div class="perso">
      <p>Monstre 1</p>
      <div class="specs">
        <div class="spe">
          <label for="monster-one-name">Nom*</label>
          <input id="monster-one-name" type="text" name="monsters[]">       
        </div>
        <div class="spe">
          <label for="monster-one-role">Type*</label>
          <select id="monster-one-role"  name="monster-role[]">
            <option value="">Choisis le rôle des monstres!</option>
            <option value="BlackMagus">Mage noir</option>
            <option value="Ork">Orque</option>
            <option value="Goblin">Gobelin</option>
            <option value="BlackShadow">Ombre noire</option>
            <option value="Sauron">Sauron!</option>
          </select>
        </div>
      </div>
    </div>


    <div class="perso">
      <p>Monstre 2</p>
      <div class="specs">
        <div class="spe">
          <label for="monster-one-name">Nom*</label>
          <input id="monster-one-name" type="text" name="monsters[]">       
        </div>
        <div class="spe">
          <label for="monster-one-role">Type*</label>
          <select id="monster-one-role"  name="monster-role[]">
            <option value="">Choisis le rôle des monstres!</option>
            <option value="BlackMagus">Mage noir</option>
            <option value="Ork">Orque</option>
            <option value="Goblin">Gobelin</option>
            <option value="BlackShadow">Ombre noire</option>
            <option value="Sauron">Sauron!</option>
          </select>
        </div>
      </div>
    </div>

    <div class="perso">
      <p>Monstre 3</p>
      <div class="specs">
        <div class="spe">
          <label for="monster-one-name">Nom*</label>
          <input id="monster-one-name" type="text" name="monsters[]">       
        </div>
        <div class="spe">
          <label for="monster-one-role">Type*</label>
          <select id="monster-one-role"  name="monster-role[]">
            <option value="">Choisis le rôle des monstres!</option>
            <option value="BlackMagus">Mage noir</option>
            <option value="Ork">Orque</option>
            <option value="Goblin">Gobelin</option>
            <option value="BlackShadow">Ombre noire</option>
            <option value="Sauron">Sauron!</option>
          </select>
        </div>
      </div>
    </div>

  </div>
</div>
<div class="envoi">
<p>Tous les champs munis d'une * sont obligatoires</p>
<input type=submit value="Commencer">
</div>


</form>



</main>  
<?php
include "components/footer.php";
?>
