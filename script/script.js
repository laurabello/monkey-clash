let drop = document.querySelector('#drop')
drop.addEventListener('click', dropAllMics)

var arrow = document.querySelectorAll('.arrow')
arrow.forEach(element => {
  element.addEventListener('click', dropZeMic)
});

function dropZeMic() {
  let mic = this.parentNode.nextElementSibling;
  if (mic.classList.contains('inactive')) {
    mic.classList.replace('inactive', 'active')
  }
  else {
    mic.classList.replace('active', 'inactive')
  }
  if (this.classList.contains('follow')){
    this.classList.remove('follow')
  }
  else {
    this.classList.add('follow')
  }
}

function dropAllMics() {
  let micsToDrop = document.querySelectorAll('.details')
  if (this.classList.contains('follow')){
    this.classList.remove('follow');
    micsToDrop.forEach(element => {
      if (element.classList.contains('active')){
        element.classList.replace('active','inactive');
      }
    })
    arrow.forEach(element => {
      if (element.classList.contains('follow')){
        element.classList.remove('follow')
      }
    })
  }
  else {
    this.classList.add('follow');
    micsToDrop.forEach(element => {
      if (element.classList.contains('inactive')) {
        element.classList.replace('inactive', 'active')
      }
    })
    arrow.forEach(element => {
      if (!(element.classList.contains('follow'))) {
        element.classList.add('follow')
      }
    })
  }
}

/*micsToDrop.forEach(element => {
  let isDropped = element.classList.contains('inactive')
  if (isDropped) {
    element.classList.replace('inactive', 'active')
  }
  else {
    element.classList.replace('active', 'inactive')
  }
})
arrow.forEach(element => {
  let isTurned = element.classList.contains('follow')
  if (isTurned) {
    element.classList.remove('follow')
  }
  else {
    element.classList.add('follow')
  }
})

  if (!micsDropped) {
    micsToDrop.forEach(element => {
      element.classList.replace('inactive', 'active');
    })
    arrow.forEach(element => {
      element.classList.add('follow');
    })
  }
  else {
    micsDropped.forEach(element => {
      element.classList.replace('active', 'inactive')
    })
    arrow.forEach(element => {
      element.classList.remove('follow');
    })
  }
}*/