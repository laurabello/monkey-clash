  <?php
    $path = "<script src=\"script/script.js\" defer></script>";
    $title = "Monkeys Clash | Issue de la bataille!";
    include "components/header.php";
    include "./assets/fight.php";
    $displayFight = fight($teamHeroes, $teamMonsters);
  ?>
  <main>
    <div class="ribbon">
    </div>
    <audio src="./medias/sounds/SFB-chimpanze-2.mp3" preload="auto" autoplay style="display: none;"></audio>
    <h2 class="subtitle">Déroulé du combat</h2>
    <div class="line-sub">
    </div>
    <div class="result">
      <button class="start-button" id="drop"> Tout voir </button>
      <div class="fight">
      <?php 
          $i =1;
          foreach ($displayFight as $turn):?>
        <div class="result-header">
          <div class="rounds-title">
            <h3>Action <?= $i; ?></h3>
            <p><span><?= $turn['rounds']['nameAttacker']?></span> attaque <span><?=$turn['rounds']['nameEnemy']?></span></p>
          </div>
          <button class="arrow"></button>
        </div>
        <div class="details inactive">
          <p>Coup porté : <span><?=$turn['rounds']['damage']?></span></p>
          <div class="rounds">
            <div class="team">
              <h4>Héros :</h4>
              <table class="state-table">
                <thead>
                  <tr>
                    <th class="main-row">Nom</th>
                    <th>Type</th>
                    <th>Niveau</th>
                    <th>Vie</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($turn['heroes'] as $hero):?>
                  <tr>
                    <td class="main-row"><?=$hero['NamePerso']?></td>
                    <td><?=$hero['RolePerso']?></td>
                    <td><?=$hero['LvlPerso']?></td>
                    <td><?=$hero['HPPerso']?></td>
                  </tr>
                <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <div class="team">
              <h4>Monstres :</h4>
              <table class="state-table">
                <thead>
                  <tr>
                    <th class="main-row">Nom</th>
                    <th>Type</th>
                    <th>Niveau</th>
                    <th>Vie</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach ($turn['monsters'] as $monster):?>
                  <tr>
                    <td class="main-row"><?=$monster['NamePerso']?></td>
                    <td><?=$monster['RolePerso']?></td>
                    <td><?=$monster['LvlPerso']?></td>
                    <td><?=$monster['HPPerso']?></td>
                  </tr>
                <?php endforeach;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      <?php 
    $i++;
    endforeach;?>
      </div>
      <h2 class="subtitle">Fin du combat</h2>
      <div class="line-sub">
      </div>
      <div class="result-announce">
        <div class="winner">
          <p class="subtitle"><?=$turn['winner'];?></p>
          <p class="winner-announce"><?=$turn['winnerSentence'];?></p>
        </div>
        <div class="story">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In placerat nisl et commodo posuere. Duis mollis erat quis lacinia hendrerit. Integer pretium mollis feugiat. Aliquam orci sem, sodales vel neque non, mollis lacinia mi. Donec sagittis felis sapien, a bibendum ipsum semper sed. Phasellus eget posuere nibh. Vivamus eu sapien diam. Vivamus volutpat risus in ante rutrum pharetra. Pellentesque maximus luctus ligula. Curabitur sem libero, dignissim in rutrum id, consequat et nisi. Nulla iaculis ornare neque vitae sagittis.</p>
          <p>Mauris facilisis, ante non lobortis tempus, erat ligula tempus metus, quis posuere erat tellus id neque. Integer sagittis ipsum ut elit imperdiet, at posuere nisl consequat. Aenean viverra euismod dapibus. Nam pharetra a odio rutrum fringilla. Curabitur magna tellus, viverra eu ultricies at, facilisis sit amet risus. Cras dapibus nisl a rutrum dictum. Mauris accumsan vulputate sem. Proin libero augue, egestas in tellus id, ultrices ornare magna. Vestibulum a mollis massa, id viverra dui. Sed malesuada nulla sed arcu malesuada malesuada. Sed purus eros, rhoncus ac sollicitudin ut, volutpat in justo. Aliquam erat volutpat. Aliquam ut sem lacus. Donec condimentum quis purus iaculis dapibus.</p>
        </div>
      </div>
    </div>   
  </main>
  <?php
    include "components/footer.php";
  ?>