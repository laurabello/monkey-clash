  <?php
    $path = "";
    $title = "Monkeys Clash | Accueil";
    include "components/header.php";
  ?>
  <main>
    <div class="background">
      <div class="introduction">
        <h2 class="subtitle">Le choc super simiesque!</h2>
        <a class="start" href="start">Commencer le combat</a>
      </div>
    </div>
  </main>
  <?php
    include "components/footer.php";
  ?>